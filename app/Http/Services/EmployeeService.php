<?php


namespace App\Http\Services;


use App\Http\Repositories\EmployeeRepository;
use App\Models\Employee;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;
use function PHPUnit\Framework\isEmpty;

class EmployeeService
{


    /**
     * @var EmployeeRepository
     */
    private $eR;
    private $data = [];

    public function __construct(EmployeeRepository $eR){
        $this->eR = $eR;
    }

    public function getDataFromUrl($url)
    {
        try{
            $request = Http::get($url);
            if($this->isSuccess($request)){
                $this->data = $request['data'];
                return $this->data;
            }
        }catch(\Exception $e){
            return ['message' => $e->getMessage()];
        }
    }

    public function isSuccess($request): bool
    {
        if($request['status'] == 'success'){
            return true;
        }
        return false;
    }

    public function insertData($data): array
    {
        if(!$this->isDataExist()){
            try{
                foreach($data as $d){
                    $employee = new Employee();
                    //debug
                    $employee->id = $d['id'];
                    //
                    $employee->employee_name = $d['employee_name'];
                    $employee->employee_salary = $d['employee_salary'];
                    $employee->employee_age = $d['employee_age'];
                    $employee->profile_image = $d['profile_image'];
                    $employee->save();
                }
                return ['status' => 'success', 'message' => 'Data insert'];
            }catch(\Exception $e){
                return ['message' => $e->getMessage()];
            }
        }
        return ['status' => 'failed', 'message' => 'Data already exist'];
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function isDataExist(): bool
    {
        if(!Employee::all()->isEmpty()){
            return true;
        }
        return false;
    }

    public function getEmployees()
    {
        return $this->eR->getEmployees();
    }

}
