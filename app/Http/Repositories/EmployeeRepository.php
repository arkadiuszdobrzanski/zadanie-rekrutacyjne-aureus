<?php


namespace App\Http\Repositories;


use App\Models\Employee;

class EmployeeRepository
{

    public function getEmployees(): array
    {
        if(!Employee::all()->isEmpty()){
            return ['data' => Employee::all(),'status' => 'success', 'message' => 'Data fetched from DB'];
        }
        return ['data' => '','status' => 'failed', 'message' => 'You should put some data in db'];

    }
}
