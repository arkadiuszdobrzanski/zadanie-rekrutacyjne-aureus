<?php

namespace App\Http\Controllers;

use App\Http\Repositories\EmployeeRepository;
use App\Http\Services\EmployeeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class APIController extends Controller
{

    public function index()
    {
        return view('welcome');
    }

    public function fetchEmployees(EmployeeService $eS)
    {
        if($eS->getDataFromUrl('https://aureus.pl/employees.json')){
            $result = $eS->insertData($eS->getData());
            return response()->json($result);
        }
    }

    public function getEmployees(EmployeeService $eS)
    {
        return $eS->getEmployees();
    }

}
