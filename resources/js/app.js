/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


import Employees from "./components/Employees";
import Home from "./components/Home";
import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

require('./bootstrap');

window.Vue = require('vue').default;


Vue.component('home-component', require('./components/Home.vue').default);
Vue.component('employees-component', require('./components/Employees.vue').default);
Vue.component('navbar-component', require('./components/Navbar.vue').default);

const routes = [
    {
        path: '/',
        component: Home,
    },
    {
        path: '/podsumowanie',
        component: Employees
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
})



const app = new Vue({
    el: '#app',
    router
});
